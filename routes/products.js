const router = require("express").Router();
const isAuth = require("../middleware/is-auth");

const {
  addProduct,
  getAllProducts,
  updateProduct,
  deleteProduct,
  getAllAdminProducts,
  getExpiredProducts
} = require("../contollers/product");

router.get("/products", getAllProducts);
router.get("/user-products", isAuth, getAllAdminProducts)
router.get("/expired-products", isAuth, getExpiredProducts)
router.post("/add-product", isAuth, addProduct);
router.post("/update-product", isAuth, updateProduct);
router.post("/delete-product", isAuth, deleteProduct);

module.exports = router;
