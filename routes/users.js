const router = require('express').Router();
const { signUp, login, resetPassword, changePassword } = require('../contollers/users')

router.post('/login', login)
router.post('/resetPassword', resetPassword)
router.post('/changePassword', changePassword)
router.post('/signup', signUp)




module.exports = router