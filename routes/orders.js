const router = require("express").Router();
const isAuth = require("../middleware/is-auth");
const { catchErrors } = require("../handlers/errorhandler");
const {
  getAllOrders,
  getProduct,
  addToCart,
  getUserCart,
  removeProductFromCart,
  addOrder,
  getAllProducts,
} = require("../contollers/shop");

router.get("/orders", isAuth, catchErrors(getAllOrders));
router.get("/product/:id", isAuth, catchErrors(getProduct));
router.post("/addToCart/:id", isAuth, catchErrors(addToCart));
router.get("/userCart", isAuth, catchErrors(getUserCart));
router.post("/removeFromCart", isAuth, catchErrors(removeProductFromCart));
router.post("/addOrder", isAuth, catchErrors(addOrder));
router.get("/allProducts", isAuth, catchErrors(getAllProducts));

module.exports = router;
