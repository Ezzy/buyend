const Product = require("../models/product");
const { validationResult } = require("express-validator");

//CREATE THE PRODUCT

exports.addProduct = async (req, res, next) => {
  const { productName, price, description, image } = req.body;
  const errors = validationResult(req);
  if (!image) {
    return res
      .status(422)
      .send({ message: "attached file is not an image, please try again" });
  }
  if (!errors.isEmpty()) {
    return res.status(422).send({ errors: errors.array() });
  }
  const product = new Product({
    productName,
    price,
    description,
    image,
    userId: req._id,
  });
  await product.save();
  return res.status(200).send({ message: "Product added successfully" });
};

//UPDATE A PRODUCT

exports.updateProduct = async (req, res, next) => {
  const productId = req.params.id;
  const { productName, price, description, image } = req.body;
  const product = await Product.findById(productId);
  if (product.userId.toString() !== req._id) {
    return res
      .status(403)
      .send({ success: "false", message: "Not authorized" });
  }
  product.productName = productName;
  product.price = price;
  product.description = description;
  product.image = image.path;
  await product.save();
  return res.status(200).send({ message: "Product updated Successfully" });
};

//DELETE A PRODUCT
exports.deleteProduct = async (req, res, next) => {
  const productId = req.params.id;
  const product = await Product.findById(productId);
  if (product.userId !== req._id) {
    return res
      .status(403)
      .send({ success: "false", message: "Not authorized" });
  }
  if (!product) {
    return res.status(404).send({ message: "No Product found" });
  }
  await Product.findByIdAndRemove(productId);
  return res.status(200).send({ message: "Product removed successfully" });
};

//GET ALL PRODUCTS

exports.getAllProducts = async (req, res, next) => {
  const products = await Product.find().select("-__v");
  return res.status(200).send(products);
};

//GET ALL ADMIN PRODUCTS
exports.getAllAdminProducts = async (req, res, next) => {
  const userProducts = await Product.find({ userId: req._id });
  if (!userProducts) {
    res.status(404).send({ message: "No products associated with user" });
  }
  return res.status(200).send(userProducts);
};

exports.getExpiredProducts = async (req, res, next) => {
  const expiredProducts = await Product.find({
    userId: req._id,
    expired: { $lt: new Date() },
  });
  return res.status(200).send(expiredProducts);
};
