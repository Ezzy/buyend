const path = require("path");
const fs = require("fs");

const Product = require("../models/product");
const User = require("../models/users");
const Orders = require("../models/orders");

//GET ALL PRODUCTS

exports.getAllProducts = async (req, res, next) => {
  try {
    const products = await Product.find({});
    return res.status(200).send(products);
  } catch (error) {
    console.log(error);
  }
};

//GET A SINGLE PRODUCT

exports.getProduct = async (req, res, next) => {
  try {
    const productId = req.params.id;
    const product = await Product.findById(productId);
    if (!product) res.status(404).send({ message: "Error fetching product" });
    return res.status(200).send(product);
  } catch (error) {
    console.log(error);
  }
};

//ADD PRODUCT TO CART

exports.addToCart = async (req, res, next) => {
  try {
    const productId = req.params.id;
    const product = await Product.findById(productId);
    if (!product)
      return res.status(404).send({ message: "Error fetching product" });
    const user = await User.findOne({ _id: req._id });
    if (!user) return res.status(404).send({ message: "Error fetching user" });
    await user.addToCart(product);
    return res.status(200).send({ message: "Item added to cart" });
  } catch (error) {
    console.log(error);
  }
};

//GET USER CART
exports.getUserCart = async (req, res, next) => {
  const user = await User.findOne({ _id: req._id });
  return res.status(200).send(user.cart.products);
};

//REMOVE ITEM FROM CART
exports.removeProductFromCart = async (req, res, next) => {
  const productId = req.params.id;
  const user = await User.findOne({ _id: req._id });
  if (!user) res.status(404).send({ message: "Error fetching user" });
  await user.removeFromCart(productId);
  res.status(200).send({ message: "Product removed from cart" });
};

//PLACE AN ORDER

exports.addOrder = async (req, res, next) => {
  //Find user
  const user = await User.findOne({ _id: req._id }).populate(
    "cart.products.productId"
  );
  const items = user.cart.products.map((item) => {
    return { quantity: item.quantity, product: { ...item.productId._doc } };
  });
  const order = new Orders({
    user: {
      email: user.email,
      userId: req._id,
    },
    products: items,
  });
  await order.save();
  res.status(200).send({ message: "Order placed successfully" });
  return user.clearCart();
};

//GET ALL ORDERS SPECIFIC TO A USER
exports.getAllOrders = async (req, res, next) => {
  const orders = await Orders.find({ "user.userId": req._id });
  return res.status(200).send(orders);
};
