const bcrypt = require("bcryptjs");
const { randomBytes } = require("crypto");
const nodemailer = require("nodemailer");
const sendGridTransport = require("nodemailer-sendgrid-transport");
const User = require("../models/users");
const { validationResult } = require("express-validator");
const jwt = require("jsonwebtoken");


const transporter = nodemailer.createTransport(
  sendGridTransport({
    auth: {
      //Lets insert our api key here
    },
  })
);

//Lets register a user

exports.signUp = async (req, res, next) => {
  const { email, password } = req.body;
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  try {
    const hashedPassword = await bcrypt.hash(password, bcrypt.genSaltSync(12));
    const cart = {
      products: [],
    };
    await User.create({ email, password: hashedPassword, cart });
    return res.status(200).send("Registration successful");
  } catch (error) {
    console.log(error);
  }
};

//Let's login

exports.login = async (req, res, next) => {
  const { email, password } = req.body;
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  try {
    const user = await User.findOne({ email });
    if (!user) {
      return res.status(404).send("User record not found");
    }
    const correctPass = await bcrypt.compare(password, user.password);
    if (!correctPass) {
      return res.status(403).send("Wrong password, try again");
    }
    const token = await jwt.sign(
      { email: user.email, _id: user._id },
      "secret",
      { expiresIn: "1hr" }
    );
    return res.status(200).send({ _id: user._id, token });
  } catch (error) {
    console.log(error);
  }
};

//RESET PASSWORD
exports.resetPassword = async () => {
  const user = await User.findOne({ email: req.body.email });
  if (!user) res.status(404).send({ message: "User not found" });
  const token = await randomBytes(20).toString("hex");
  user.resetToken = token;
  user.resetTokenExpiration = Date.now() + 3600000; //1 HOUR
  await user.save();
  transporter.sendMail({
    to: req.body.email,
    from: "buyend@gmail.com",
    subject: "Password Reset",
    html: `<p>You requested a password reset</p>
                      <p>Please click this <a href="/https://localhost:4001/reset/${token}">link</a> to set a new password</p>
                 `,
  });
  return res.status(200).send({ message: "Password link sent successfully" });
};

//CHANGE USER PASSWORD
exports.changePassword = async (req, res, next) => {
  const { oldPassword, newPassword, email } = req.body;
  const user = await User.findOne({ email });
  if (!user) res.status(404).send({ message: "No user record found" });
  const isCorrectPassword = await User.comparePassword(
    oldPassword,
    user.password
  );
  if (!isCorrectPassword)
    res.status(404).send({ message: "Incorrect password" });
  user.password = newPassword;
  await user.save();
  return res.status(200).send({ message: "Password changed successfully" });
};


