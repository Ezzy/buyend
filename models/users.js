const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const bcrypt = require("bcryptjs");

const userSchema = new Schema({
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },

  resetToken: {
    type: String,
  },
  resetTokenExpiration: Date,
  cart: {
    products: [
      {
        productId: {
          type: Schema.Types.ObjectId,
          required: true,
          ref: "Product",
        },
        quantity: {
          type: Number,
          required: true,
        },
      },
    ],
  },
});

//Method to add a product to a cart

userSchema.methods.addToCart = async function (product) {
  cartProductIndex = this.cart.products.findIndex((cartProduct) => {
    return cartProduct.productId.toString() === product._id.toString();
  });
  let newQuantity = 1;
  const updatedCartItems = [...this.cart.products];

  if (cartProductIndex >= 0) {
    newQuantity = this.cart.products[cartProductIndex].quantity + 1;
    updatedCartItems[cartProductIndex].quantity = newQuantity;
  } else {
    updatedCartItems.push({
      productId: product._id,
      quantity: newQuantity,
    });
  }
  this.cart = {
    products: updatedCartItems,
  };
  await this.save();
};

//Remove item from cart

userSchema.methods.removeFromCart = async function (id) {
  const filteredProducts = this.cart.products.filter((product) => {
    return product.productId.toString() !== id.toString();
  });
  this.cart = {
    products: filteredProducts,
  };
  await this.save();
};

//Clear cart

userSchema.methods.clearCart = async function () {
  this.cart = {
    products: [],
  };
  await this.save();
};

userSchema.statics.comparePassword = async (password, userPassword) => {
  await bcrypt.compare(password, userPassword);
};

// userSchema.pre("save", function saveHook(next) {
//   const user = this;
//   if (!user.isModified("password")) return next();
//   const saltRounds = 10;
//   const salt = bcrypt.genSaltSync(saltRounds);
//   const hash = bcrypt.hashSync(user.password, salt);
//   user.password = hash;
//   return next();
// });

module.exports = mongoose.model("user", userSchema);
