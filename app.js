require("dotenv").config();
const express = require("express");
const app = express();
const mongoose = require("mongoose");
const path = require("path");
const cors = require("cors");
const bodyParser = require("body-parser");
const multer = require("multer");
const {
  productionErros,
  developmentErrors,
} = require("./handlers/errorhandler");

//Routes
const orderRoutes = require("./routes/orders");
const productRoutes = require("./routes/products");
const authRoutes = require("./routes/users");

//HANDLE FILE STORAGE

const fileStore = multer.diskStorage({
  destination: (req, res, cb) => {
    cb(null, path.join(__dirname, "images"));
  },
  filename: (req, file, cb) => {
    cb(null, `${new Date().getTime()}- ${file.originalname}`);
  },
});

const fileFilter = (req, file, cb) => {
  if (file.mimetype === ("image/jpg" || "image/jpeg" || "image/png")) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

app.use(bodyParser.json());

//ROUTES MIDDLEWARES
app.use("/auth", authRoutes);
app.use(orderRoutes);
app.use(productRoutes);

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

//CORS MIDDLEWARE
app.use(cors());

//SET UP MULTER FOR FILE STORAGE

if (app.get("env") === "development") {
  app.use(developmentErrors);
}

//production error handler
app.use(productionErros);

app.use(multer({ storage: fileStore, fileFilter }).single("image"));

//HANDLE ERROR ROUTES OR PAGES
app.use((req, res, next) => {
  res.status(404).send("<h1>Page not found</h1>");
  next();
});

//CONNECT TO LOCAL DB

mongoose
  .connect("mongodb://127.0.0.1:27017/buyend", { useNewUrlParser: true })
  .then(() => console.log("Database connected successfully"))
  .catch((err) => console.log(err));

//SPIN UP SERVER
app.listen(4001, () => console.log("Server running"));
